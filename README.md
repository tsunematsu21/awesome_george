# Awesome George #
 Version0.1.1

# Overview
George is awesome! POW!

# Getting Started #

Clone Repository  
```
git clone https://bitbucket.org/t-montgomery/awesome_george.git
```

Install gem  
```
cd awesome_george
bundle install --path vendor/bundle
```

Migration  
```
bundle exec rake db:migrate
```

Start the web server  
```
bundle exec rails s
```

# Contribution guidelines #

1. Fork it ( https://bitbucket.org/t-montgomery/awesome_george/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Pull Request
