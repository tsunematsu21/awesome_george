require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#email' do
    it { should validate_presence_of(:email) }
    it { should allow_value('test@test.com').for(:email) }
    it { should !allow_value('test@test').for(:email) }
  end

  describe '#username' do
    it { should validate_presence_of(:username) }
    it { should allow_value('user').for(:username) }
    it { should validate_uniqueness_of(:username) }
  end

  describe '#encrypted_password' do
    it { should validate_presence_of(:password) }
    it { should ensure_length_of(:password).is_at_least(8) }
  end
end
