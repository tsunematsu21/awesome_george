require 'rails_helper'

RSpec.describe AmazingWord, type: :model do
  describe '#word' do
    it { should validate_presence_of(:word) }
    it { should allow_value('George is not Google').for(:word) }
  end

  describe '#pronunciation' do
    it { should validate_presence_of(:pronunciation) }
    it { should allow_value('じょーじ いず のっと ぐーぐる').for(:pronunciation) }
    it { should !allow_value('George is not Google').for(:pronunciation) }
  end

  describe '#description' do
    it { should validate_presence_of(:description) }
  end
end
