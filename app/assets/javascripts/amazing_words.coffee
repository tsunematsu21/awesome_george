# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $container = $('#amazing-words')

  #$container.imagesLoaded ->
  $container.masonry
    itemSelector: '.amazing-word'
    isFitWidth: true

  $container.infinitescroll
    navSelector  : 'nav.pagination',
    nextSelector : 'nav.pagination a[rel=next]',
    itemSelector : '.amazing-word',
    loading: {
      finishedMsg: 'No more pages to load.',
      img: 'http://i.imgur.com/6RMhx.gif'
    },
    ( newElements ) ->
      $newElems = $( newElements ).css({ opacity: 0 })
      #$newElems.imagesLoaded ->
      $newElems.animate({ opacity: 1 });
      $container.masonry( 'appended', $newElems, true )


