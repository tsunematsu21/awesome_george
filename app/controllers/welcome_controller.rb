class WelcomeController < ApplicationController
  def index
    @amazing_words = AmazingWord.page(params[:page]).per(5)
  end
end
