class AmazingWordsController < ApplicationController
  def index
    #@amazing_words = AmazingWord.find_by_sql('SELECT * FROM amazing_words ORDER BY RANDOM()').page(params[:page]).per(30)
    @amazing_words = Kaminari.paginate_array(AmazingWord.all.shuffle).page(params[:page]).per(30)

  end

  def show
    @amazing_word = AmazingWord.find(params[:id])
  end

  def new
    @amazing_word = current_user.posted_words.build
  end

  def create
    @amazing_word = current_user.posted_words.build(amazing_word_params)
    if @amazing_word.save
      redirect_to @amazing_word, notice: '作成しました'
    else
      render :new
    end
  end

  def edit
    @amazing_word = current_user.posted_words.find(params[:id])
  end

  def update
    @amazing_word = current_user.posted_words.find(params[:id])
    if @amazing_word.update(amazing_word_params)
      redirect_to @amazing_word, notice: '更新しました'
    else
      render :edit
    end
  end

  private

  def amazing_word_params
    params.require(:amazing_word).permit!
  end
end
