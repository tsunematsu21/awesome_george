class AmazingWord < ActiveRecord::Base
  belongs_to :contributor, class_name: 'User'

  validates :word, presence: true
  validates :pronunciation, presence: true, format: {with: /\A[\p{hiragana}|ー| |　]+?\z/, message: 'はひらがなで入力してください'}
  validates :description, presence: true
end
