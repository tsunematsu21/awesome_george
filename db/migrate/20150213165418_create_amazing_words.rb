class CreateAmazingWords < ActiveRecord::Migration
  def change
    create_table :amazing_words do |t|
      t.integer :contributor_id
      t.string  :word,           null: false
      t.string  :pronunciation,  null: false
      t.text    :description,    null: false

      t.timestamps null: false
    end

    add_index :amazing_words, :contributor_id
  end
end
